﻿//A a3 = new A(123, "haha");
//Console.WriteLine(a3.a); // kết quả o.a là?
//Console.WriteLine(a3.b); // kết quả o.b là?

//A a4 = new A(3000, "Huy dep trai");
//Console.WriteLine(a4.a); // kết quả b.a là?
//Console.WriteLine(a4.b); // kết quả b.b là?

//class A
//{
//    public int a { get; set; }
//    public string b { get; set; }
//    public A(int _a, string _b)
//    {
//        a = _a;
//        b = _b;
//    }
//}


//using ConsoleApp6;

//Employee emp1 = new Employee(); 
//Employee emp2 = new Employee(101, "Thuw"); 

//Console.WriteLine("Thông tin Employee 1:");
//emp1.DisplayInfo();

//Console.WriteLine("\nThông tin Employee 2:");
//emp2.DisplayInfo();



using ConsoleApp6;

EmployeeManagement employeeManagementUI = new EmployeeManagement();
string choice = string.Empty;
while (choice != "4")
{
    Console.WriteLine("*********** Menu ***********");
    Console.WriteLine("1. Xem thông tin nhân viên");
    Console.WriteLine("2. Thêm nhân viên mới");
    Console.WriteLine("3. Xóa nhân viên");
    Console.WriteLine("4. Thoát");

    Console.Write("Lựa chọn của bạn: ");
    choice = Console.ReadLine();

    switch (choice)
    {
        case "1":
            employeeManagementUI.ViewEmployeeInformation();
            break;

        case "2":
            employeeManagementUI.AddNewEmployee();
            break;

        case "3":
            employeeManagementUI.RemoveEmployee();
            break;
    }
}
Console.WriteLine("End");




