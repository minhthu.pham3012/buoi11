﻿
class Person
{
    public string Name { get; set; }

    public void Drive(Car car)
    {
        Console.WriteLine($"{Name} is driving {car.Brand} {car.Model}");
    }
}
