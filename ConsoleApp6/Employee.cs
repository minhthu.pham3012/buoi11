﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    public class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Position { get; set; }
        public double Salary { get; set; }

        //public Employee()
        //{

        //    Age = 20;
        //    Salary = 5000;
        //}

        //public Employee(int id, string name)
        //{
        //    Id = id;
        //    Name = name;
        //    Age = 0;
        //    Salary = 0;
        //}

        //public void DisplayInfo()
        //{
        //    Console.WriteLine($"ID: {Id}, Name: {Name}, Age: {Age}, Salary: {Salary}");
        //}


        public Employee(int id, string name, string position, double salary)
        {
            Id = id;
            Name = name;
            Position = position;
            Salary = salary;
        }

        public override string ToString()
        {
            return $"ID: {Id}, Name: {Name}, Position: {Position}, Salary: {Salary}";
        }
    }
   
}

