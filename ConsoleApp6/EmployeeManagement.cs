﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    public class EmployeeManagement
    {
        static List<Employee> employees = new List<Employee>();
        public void ViewEmployeeInformation()
        {
            Console.WriteLine("Employee Information:");
            if (employees.Count == 0)
            {
                Console.WriteLine("No employees to display.");
            }
            else
            {
                foreach (var employee in employees)
                {
                    Console.WriteLine(employee);
                }
            }
        }

        public void AddNewEmployee()
        {
            Console.WriteLine("Enter Employee ID: ");
            int id = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter Employee Name: ");
            string name = Console.ReadLine();

            Console.WriteLine("Enter Employee Position: ");
            string position = Console.ReadLine();

            Console.WriteLine("Enter Employee Salary: ");
            double salary = Convert.ToDouble(Console.ReadLine());

            employees.Add(new Employee(id, name, position, salary));
            Console.WriteLine("Employee added successfully.");
        }

        public void RemoveEmployee()
        {
            Console.Write("Enter Employee ID to remove: ");
            int id = Convert.ToInt32(Console.ReadLine());

            var employeeToRemove = employees.Find(e => e.Id == id);
            if (employeeToRemove != null)
            {
                employees.Remove(employeeToRemove);
                Console.WriteLine("Employee removed successfully.");
            }
            else
            {
                Console.WriteLine("Employee not found.");
            }
        }
    }
}